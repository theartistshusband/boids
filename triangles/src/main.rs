use std::process;

use nannou::prelude::*;
use tahga::pursuit::boids::Boids;

const WIDTH: u32 = 600;
const HEIGHT: u32 = 600;
const CAPTURE: bool = false;
const BOID_GROUP_SIZE: u32 = 3;
const NUM_BOID_GROUPS: u32 = 5;
const NUM_BOIDS: u32 = NUM_BOID_GROUPS * BOID_GROUP_SIZE;

fn main() {
    nannou::app(model)
        .update(update)
        .run();
}

struct Model {
    boids: Boids,

    paused: bool,
    ctrl_key_pressed: bool,
}

fn model(app: &App) -> Model {
    app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .key_released(key_released)
        .build()
        .unwrap();

    let rect = app.window_rect();

    let mut boids = Boids::new();
    for i in 0..NUM_BOID_GROUPS {
        let id = i * 3;
        for j in 0..3 {
            let position = vec2(
                random_range(rect.left(), rect.right()),
                random_range(rect.bottom(), rect.top()),
            );
            let velocity = vec2(
                random_range(-10., 10.),
                random_range(-10., 10.),
            );
            let boid = boids.new_boid(position, velocity).color(hsla(id as f32 / NUM_BOIDS as f32, 0.3, 0.3, 0.3));
            boids.add_boid(boid);
        }
    }

    Model {
        boids,

        paused: false,
        ctrl_key_pressed: false,
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if model.paused { return; }

    let rect = app.window_rect();
    for i in 0..model.boids.boids.len() {
        let mut boid = model.boids.boids[i].clone();
        boid.update(&model.boids.boids, &rect);
        model.boids.boids[i] = boid;
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    if model.paused { return; }
    let draw = app.draw();

    // if app.elapsed_frames()  == 1 {
    draw.background().hsla(1., 1., 1., 0.1);
    // }

    for group in 0..NUM_BOID_GROUPS {
        let i = (group * BOID_GROUP_SIZE) as usize;
        let color = model.boids.boids[i].color;
        let mut points = Vec::new();
        for ip in 0..BOID_GROUP_SIZE as usize {
            points.push(model.boids.boids[i + ip].position)
        }
        draw.polygon()
            .color(STEELBLUE)
            .points(points.clone())
            .color(color);
        for ip in 1..BOID_GROUP_SIZE as usize {
            draw.line()
                .start(points[ip - 1])
                .end(points[ip]);
        }
        draw.line()
            .start(points[BOID_GROUP_SIZE as usize - 1])
            .end(points[0]);
    }

    draw.to_frame(app, &frame).unwrap();

    if CAPTURE {
        let file_path = captured_frame_path(app, &frame);
        app.main_window().capture_frame(file_path);
    }
}

/// React to key-presses
fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::C => {
            if model.ctrl_key_pressed {
                process::exit(0);
            }
        }
        Key::S => {
            let file_path = saved_image_path(app);
            app.main_window().capture_frame(file_path);
        }
        Key::Space => {
            model.paused = !model.paused;
        }
        Key::LControl => {
            model.ctrl_key_pressed = true;
        }
        _other_key => {}
    }
}

/// React to key releases
fn key_released(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::LControl => {
            model.ctrl_key_pressed = false;
        }
        _other_key => {}
    }
}

/// Get the path to the next captured frame
fn captured_frame_path(app: &App, frame: &Frame) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("frames")
        .join(format!("frame{:05}", frame.nth()))
        .with_extension("png")
}

/// Get the path to the next saved image
fn saved_image_path(app: &App) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("saved")
        .join(format!("image{:05}", chrono::offset::Local::now()))
        .with_extension("png")
}
