use std::process;

use nannou::prelude::*;
use tahga::pursuit::boids::Boids;

const WIDTH: u32 = 1200;
const HEIGHT: u32 = 1200;
const CAPTURE: bool = false;
const NUM_BOIDS: u32 = 100;

fn main() {
    nannou::app(model)
        .update(update)
        .run();
}

struct Model {
    boids: Boids,

    paused: bool,
    ctrl_key_pressed: bool,
}

fn model(app: &App) -> Model {
    app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .key_released(key_released)
        .build()
        .unwrap();

    let rect = app.window_rect();

    let mut boids = Boids::new();
    for id in 0..NUM_BOIDS {
        let position = vec2(
            random_range(rect.left(), rect.right()),
            random_range(rect.bottom(), rect.top()),
        );
        let velocity = vec2(
            random_range(-10., 10.),
            random_range(-10., 10.),
        );
        boids.add_boid(boids.new_boid(position, velocity).color(hsla(id as f32 / NUM_BOIDS as f32, 1., 0.3, 0.7)));
    }

    Model {
        boids,

        paused: false,
        ctrl_key_pressed: false,
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if model.paused { return; }
    model.boids.update(app);
}

fn view(app: &App, model: &Model, frame: Frame) {
    if model.paused { return; }
    let draw = app.draw();

    draw.background().color(WHITE);
    &model.boids.boids.iter().map(|b| {
        //b.view(&draw)
        draw.ellipse()
            .color(b.color)
            .w(5.)
            .h(5.)
            .xy(b.position);
        let mut p0 = b.position.clone();
        for p in b.path.iter().rev() {
            let p1 = p.clone();
            draw.line()
                .start(p0)
                .end(p1)
                .color(b.color);
            p0 = p1;
        }
    }
    ).collect::<Vec<_>>();

    draw.to_frame(app, &frame).unwrap();

    if CAPTURE {
        let file_path = captured_frame_path(app, &frame);
        app.main_window().capture_frame(file_path);
    }
}

/// React to key-presses
fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::C => {
            if model.ctrl_key_pressed {
                process::exit(0);
            }
        }
        Key::S => {
            let file_path = saved_image_path(app);
            app.main_window().capture_frame(file_path);
        }
        Key::Space => {
            model.paused = !model.paused;
        }
        Key::LControl => {
            model.ctrl_key_pressed = true;
        }
        _other_key => {}
    }
}

/// React to key releases
fn key_released(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::LControl => {
            model.ctrl_key_pressed = false;
        }
        _other_key => {}
    }
}

/// Get the path to the next captured frame
fn captured_frame_path(app: &App, frame: &Frame) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("frames")
        .join(format!("frame{:05}", frame.nth()))
        .with_extension("png")
}

/// Get the path to the next saved image
fn saved_image_path(app: &App) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("saved")
        .join(format!("image{:05}", chrono::offset::Local::now()))
        .with_extension("png")
}
